import React from 'react'
import { Provider } from 'react-redux'
import App, { Container } from 'next/app'
import wat from '../redux/store'
import Router from 'next/router';
import withRedux from 'next-redux-wrapper'
import NProgress from 'nprogress';
import "bootstrap/dist/css/bootstrap.min.css"
import "nprogress/nprogress.css";

Router.onRouteChangeStart = () => {
    NProgress.start();
};

Router.onRouteChangeComplete = () => {
    NProgress.done();
};

class MyApp extends App {
    static async getInitialProps({ Component, ctx }) {
        ctx.store.dispatch({ type: 'FOO', payload: 'foo' });
        const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
        return { pageProps };
    }

    render() {
        const { Component, pageProps, store } = this.props;
        return (
            <Container>
                <Provider store={store}>
                    <Component {...pageProps} />
                </Provider>
            </Container>
        );
    }

}

export default withRedux(wat)(MyApp);