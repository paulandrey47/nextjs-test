import React, { useState } from 'react'
import { Form, Button, Card } from 'react-bootstrap'

function ContactForm() {
    const [contacts, setContacts] = useState([]);

    const [contact, setContact] = useState({
        email: '',
        name: '',
        message: ''
    })

    const onSubmit = async e => {
        e.preventDefault();
        await setContacts([
            ...contacts,
            contact
        ])
        await setContact({
            email: '',
            name: '',
            message: ''
        })
        console.log(contacts)
    }

    const onChange = e => {
        setContact({
            ...contact,
            [e.target.name]: e.target.value
        })
    }

    return (
        <>
            <Form className="mt-3" onSubmit={onSubmit}>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" name="email" placeholder="Enter email" onChange={onChange} value={contact.email} />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                </Form.Text>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="text" name="name" placeholder="Enter Your Name" onChange={onChange} value={contact.name} />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Message</Form.Label>
                    <Form.Control as="textarea" rows="3" name="message" onChange={onChange} value={contact.message} />
                </Form.Group>

                <Form.Group>
                    <Button variant="outline-success" type="submit">
                        Submit
                </Button>
                </Form.Group>
            </Form>
            {
                contacts.map((contact, index) => {
                    return (
                        <Card key={index} className="mb-2">
                            <Card.Body>
                                {contact.message}
                            </Card.Body>
                        </Card>
                    )
                })
            }
        </>
    )
}

export default ContactForm;