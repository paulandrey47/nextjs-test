import React from 'react'
import Todo from './Todo'

export default function index(props) {
    const { todos } = props;
    return (
        <div>
            <p>Those Todos...</p>
            <div className="row">
                {
                    todos.map(todo => (<Todo todo={todo} key={todo.id} />))
                }
            </div>
        </div>
    )
}
