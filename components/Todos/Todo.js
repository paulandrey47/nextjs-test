import React from 'react';
import Link from 'next/link';
import { Card, Button } from 'react-bootstrap';
import { connect } from 'react-redux'
import { deleteTodo, toggleCompleteTodo } from '../../redux/action/todoActions'

function Todo(props) {
    const onClickToggle = async id => {
        await props.toggleCompleteTodo(id);
    }

    const onClickDelete = async id => {
        await props.deleteTodo(id);
    }

    const { todo } = props;
    return (
        <div className="col-12 my-2">
            <Card>
                <Card.Body>
                    <Link href="/todos/[id]" as={`/todos/${todo.id}`}>
                        <a> {todo.title}</a>
                    </Link>
                    <div className="float-right">
                        {
                            todo.completed ? (
                                <Button variant="outline-danger mr-2" onClick={() => onClickToggle(todo.id)}>
                                    Mark as Incomplete
                                </Button>
                            ) : (
                                    <Button variant="outline-success mr-2" onClick={() => onClickToggle(todo.id)}>
                                        Mark as Complete
                                    </Button>
                                )
                        }
                        <Button variant="outline-danger" onClick={() => onClickDelete(todo.id)}>
                            Delete
                        </Button>
                    </div>
                </Card.Body>
            </Card>
        </div>
    )
}

export default connect(null, { deleteTodo, toggleCompleteTodo })(Todo)