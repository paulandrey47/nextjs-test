import axios from 'axios';

export const GET_TODOS = 'GET_TODOS';
export const GET_TODO = 'GET_TODO';
export const DELETE_TODO = 'DELETE_TODO';
export const TOGGLE_COMPLETE_TODO = 'TOGGLE_COMPLETE_TODO';

export const getTodos = () => async dispatch => {
    try {
        const url = 'https://jsonplaceholder.typicode.com/todos';
        const { data } = await axios.get(url)
        dispatch({
            type: GET_TODOS,
            payload: data
        })
    } catch (e) {
        console.log(e)
    }
}

export const getTodo = id => async dispatch => {
    try {
        const url = 'https://jsonplaceholder.typicode.com/todos/' + id;
        const { data } = await axios.get(url)
        dispatch({
            type: GET_TODO,
            payload: data
        })
    } catch (e) {
        console.log(e)
    }
}

export const deleteTodo = id => async dispatch => {
    try {
        dispatch({
            type: DELETE_TODO,
            payload: id
        })
    } catch (e) {
        console.log(e);
    }
}

export const toggleCompleteTodo = id => async dispatch => {
    try {
        dispatch({
            type: TOGGLE_COMPLETE_TODO,
            payload: id
        })
    } catch (e) {
        console.log(e);
    }
}